-- 创建表空间
CREATE TABLESPACE TBS1 DATAFILE 'tbs1_datafile.dbf' SIZE 2G;
CREATE TABLESPACE TBS2 DATAFILE 'tbs2_datafile.dbf' SIZE 2G;

-- 创建商品信息表
CREATE TABLE Products (
ProductID NUMBER PRIMARY KEY,
ProductName VARCHAR2(100),
ProductDescription VARCHAR2(500),
Price NUMBER,
Stock NUMBER
) TABLESPACE TBS1;

-- 创建客户信息表
CREATE TABLE Customers (
CustomerID NUMBER PRIMARY KEY,
CustomerName VARCHAR2(100),
Address VARCHAR2(200),
PhoneNumber VARCHAR2(20)
) TABLESPACE TBS1;

-- 创建订单信息表
CREATE TABLE Orders (
OrderID NUMBER PRIMARY KEY,
CustomerID NUMBER REFERENCES Customers(CustomerID),
OrderDate DATE,
TotalAmount NUMBER
) TABLESPACE TBS2;

-- 创建订单明细表
CREATE TABLE OrderDetails (
DetailID NUMBER PRIMARY KEY,
OrderID NUMBER REFERENCES Orders(OrderID),
ProductID NUMBER REFERENCES Products(ProductID),
Quantity NUMBER,
UnitPrice NUMBER
) TABLESPACE TBS2;

-- 创建用户1并分配权限
CREATE USER USER1 IDENTIFIED BY password DEFAULT TABLESPACE TBS1;
GRANT CONNECT, RESOURCE TO USER1;
GRANT SELECT, INSERT, UPDATE, DELETE ON Products TO USER1;
GRANT SELECT, INSERT, UPDATE, DELETE ON Customers TO USER1;

-- 创建用户2并分配权限
CREATE USER USER2 IDENTIFIED BY password DEFAULT TABLESPACE TBS2;
GRANT CONNECT, RESOURCE TO USER2;
GRANT SELECT, INSERT, UPDATE, DELETE ON Orders TO USER2;
GRANT SELECT, INSERT, UPDATE, DELETE ON OrderDetails TO USER2;


CREATE OR REPLACE PACKAGE BODY SalesPackage AS
  PROCEDURE CreateOrder(
    p_CustomerID IN NUMBER,
    p_ProductID  IN NUMBER,
    p_Quantity   IN NUMBER
  ) AS
    v_OrderID  NUMBER;
    v_UnitPrice NUMBER;
    v_TotalAmount NUMBER;
  BEGIN
    -- 获取商品单价
    SELECT Price INTO v_UnitPrice FROM Products WHERE ProductID = p_ProductID;
    
    -- 插入订单信息
    INSERT INTO Orders (OrderID, CustomerID, OrderDate, TotalAmount)
    VALUES (ORDER_SEQ.NEXTVAL, p_CustomerID, SYSDATE, 0);
    
    -- 获取新插入的订单ID
    SELECT ORDER_SEQ.CURRVAL INTO v_OrderID FROM DUAL;
    
    -- 插入订单明细
    INSERT INTO OrderDetails (DetailID, OrderID, ProductID, Quantity, UnitPrice)
    VALUES (DETAIL_SEQ.NEXTVAL, v_OrderID, p_ProductID, p_Quantity, v_UnitPrice);
    
    -- 更新订单总金额
    SELECT SUM(Quantity * UnitPrice) INTO v_TotalAmount
    FROM OrderDetails WHERE OrderID = v_OrderID;
    
    UPDATE Orders SET TotalAmount = v_TotalAmount WHERE OrderID = v_OrderID;
    
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END CreateOrder;
  
  PROCEDURE CancelOrder(
    p_OrderID IN NUMBER
  ) AS
    v_ProductID NUMBER;
    v_Quantity  NUMBER;
  BEGIN
    -- 获取订单明细信息
    SELECT ProductID, Quantity INTO v_ProductID, v_Quantity
    FROM OrderDetails WHERE OrderID = p_OrderID;
    
    -- 恢复商品库存数量
    UPDATE Products SET Stock = Stock + v_Quantity WHERE ProductID = v_ProductID;
    
    -- 删除订单及订单明细
    DELETE FROM OrderDetails WHERE OrderID = p_OrderID;
    DELETE FROM Orders WHERE OrderID = p_OrderID;
    
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      RAISE;
  END CancelOrder;
  
  FUNCTION CalculateTotalAmount(
    p_OrderID IN NUMBER
  ) RETURN NUMBER AS
    v_TotalAmount NUMBER;
  BEGIN
    -- 计算订单总金额
    SELECT TotalAmount INTO v_TotalAmount FROM Orders WHERE OrderID = p_OrderID;
    
    RETURN v_TotalAmount;
  EXCEPTION
    WHEN OTHERS THEN
      RAISE;
  END CalculateTotalAmount;
  
END SalesPackage;



-- 执行完全备份
RMAN> BACKUP DATABASE;

-- 执行增量备份
RMAN> BACKUP INCREMENTAL LEVEL 1 DATABASE;

-- 执行日志备份
RMAN> BACKUP ARCHIVELOG ALL;

-- 还原数据库到指定时间点
RMAN> RUN {
SET UNTIL TIME 'YYYY-MM-DD HH24:MI:SS';
RESTORE DATABASE;
RECOVER DATABASE;
}